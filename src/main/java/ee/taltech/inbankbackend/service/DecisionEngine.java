package ee.taltech.inbankbackend.service;

import com.github.vladislavgoltjajev.personalcode.locale.estonia.EstonianPersonalCodeValidator;
import ee.taltech.inbankbackend.config.DecisionEngineConstants;
import ee.taltech.inbankbackend.exceptions.*;
import ee.taltech.inbankbackend.utils.AgeValidator;
import org.springframework.stereotype.Service;

/**
 * A service class that provides a method for calculating an approved loan amount and period for a customer.
 * The loan amount is calculated based on the customer's credit modifier,
 * which is determined by the last four digits of their ID code.
 */
@Service
public class DecisionEngine {

    // Used to check for the validity of the presented ID code.
    private final EstonianPersonalCodeValidator personalCodeValidator = new EstonianPersonalCodeValidator();
    private final AgeValidator ageValidator = new AgeValidator();
    private int creditModifier = 0;

    /**
     * Calculates the maximum loan amount and period for the customer based on their ID code,
     * the requested loan amount and the loan period.
     * The loan period must be between 12 and 60 months (inclusive).
     * The loan amount must be between 2000 and 10000€ months (inclusive).
     *
     * @param personalCode ID code of the customer that made the request.
     * @param loanAmount Requested loan amount
     * @param loanPeriod Requested loan period
     * @return A Decision object containing the approved loan amount and period, and an error message (if any)
     * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
     * @throws InvalidAgeException If the age provided from personal ID code is invalid
     * @throws InvalidLoanAmountException If the requested loan amount is invalid
     * @throws InvalidLoanPeriodException If the requested loan period is invalid
     * @throws NoValidLoanException If there is no valid loan found for the given ID code, loan amount and loan period
     */
    public Decision calculateApprovedLoan(String personalCode, long loanAmount, int loanPeriod)
            throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException,
            NoValidLoanException, InvalidAgeException {

        verifyInputs(personalCode, loanAmount, loanPeriod);

        creditModifier = getCreditModifier(personalCode);
        int outputLoanAmount, highestLoanAmount = getHighestValidLoanAmount(loanPeriod);

        while (!isLoanAmountBiggerOrEqualToMin(highestLoanAmount)) {
            highestLoanAmount = getHighestValidLoanAmount(++loanPeriod);
        }

        if (!isLoanPeriodInRange(loanPeriod)) {
            throw new NoValidLoanException("No valid loan found!");
        }

        outputLoanAmount = Math.min(DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT, highestLoanAmount);

        return new Decision(outputLoanAmount, loanPeriod, null);
    }

    /**
     * Calculates the largest valid loan for the current credit modifier and loan period.
     *
     * @return Largest valid loan amount
     */
    private int getHighestValidLoanAmount(int loanPeriod) {
        return creditModifier * loanPeriod;
    }

    /**
     * Calculates the credit modifier of the customer to according to the last four digits of their ID code.
     * Debt - 0000...2499
     * Segment 1 - 2500...4999
     * Segment 2 - 5000...7499
     * Segment 3 - 7500...9999
     *
     * @param personalCode ID code of the customer that made the request.
     * @return Segment to which the customer belongs.
     */
    private int getCreditModifier(String personalCode) throws NoValidLoanException {
        int segment = Integer.parseInt(personalCode.substring(personalCode.length() - 4));

        if (segment < 2500) {
            throw new NoValidLoanException("No valid loan found!");
        } else if (segment < 5000) {
            return DecisionEngineConstants.SEGMENT_1_CREDIT_MODIFIER;
        } else if (segment < 7500) {
            return DecisionEngineConstants.SEGMENT_2_CREDIT_MODIFIER;
        }

        return DecisionEngineConstants.SEGMENT_3_CREDIT_MODIFIER;
    }

    /**
     * Verify that all inputs are valid according to business rules.
     * If inputs are invalid, then throws corresponding exceptions.
     *
     * @param personalCode Provided personal ID code
     * @param loanAmount Requested loan amount
     * @param loanPeriod Requested loan period
     * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
     * @throws InvalidLoanAmountException If the requested loan amount is invalid
     * @throws InvalidLoanPeriodException If the requested loan period is invalid
     */
    private void verifyInputs(String personalCode, long loanAmount, int loanPeriod)
            throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException, InvalidAgeException {
        if (!isPersonalCodeValid(personalCode)) {
            throw new InvalidPersonalCodeException("Invalid personal ID code!");
        }
        if (!isAgeInRange(personalCode)) {
            throw new InvalidAgeException("Invalid age of person!");
        }
        if (!isLoanAmountInRange(loanAmount)) {
            throw new InvalidLoanAmountException("Invalid loan amount!");
        }
        if (!isLoanPeriodInRange(loanPeriod)) {
            throw new InvalidLoanPeriodException("Invalid loan period!");
        }
    }

    /**
     * Verify that input is valid according to country laws. If so, return true. If otherwise, return false.
     *
     * @param personalCode Provided personal ID code
     * @return answer to the question "is personal code valid?"
     */
    private boolean isPersonalCodeValid(String personalCode) {
        return personalCodeValidator.isValid(personalCode);
    }

    /**
     * Verify that input is valid according to business rules. If so, return true. If otherwise, return false.
     *
     * @param personalCode Provided personal ID code
     * @return answer to the question "is age of the person taken from personal code valid?"
     * @throws InvalidPersonalCodeException will be thrown if personal code cannot provide age of the person
     */
    private boolean isAgeInRange(String personalCode) throws InvalidPersonalCodeException {
        int age = ageValidator.getAge(personalCode);
        return DecisionEngineConstants.MINIMUM_AGE <= age && age <= DecisionEngineConstants.MAXIMUM_AGE;
    }

    /**
     * Verify that input (loan amount) is valid according to business rules.
     * If so, return true. If otherwise, return false.
     *
     * @param loanAmount Requested loan amount
     * @return answer to the question "is loan amount valid?"
     */
    private boolean isLoanAmountInRange(long loanAmount) {
        return isLoanAmountBiggerOrEqualToMin(loanAmount) && isLoanAmountSmallerOrEqualToMax(loanAmount);
    }

    /**
     * Verify that input (loan amount) is valid according to business rules.
     * If so, return true. If otherwise, return false.
     *
     * @param loanAmount Requested loan amount
     * @return answer to the question "is loan amount bigger or equal to minimum loan amount?"
     */
    private boolean isLoanAmountBiggerOrEqualToMin(long loanAmount) {
        return DecisionEngineConstants.MINIMUM_LOAN_AMOUNT <= loanAmount;
    }

    /**
     * Verify that input (loan amount) is valid according to business rules.
     * If so, return true. If otherwise, return false.
     *
     * @param loanAmount Requested loan amount
     * @return answer to the question "is loan amount smaller or equal to maximum loan amount?"
     */
    private boolean isLoanAmountSmallerOrEqualToMax(long loanAmount) {
        return loanAmount <= DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT;
    }

    /**
     * Verify that input (loan period) is valid according to business rules.
     * If so, return true. If otherwise, return false.
     *
     * @param loanPeriod Requested loan period
     * @return answer to the question "is loan period valid?"
     */
    private boolean isLoanPeriodInRange(int loanPeriod) {
        return DecisionEngineConstants.MINIMUM_LOAN_PERIOD <= loanPeriod
                && loanPeriod <= DecisionEngineConstants.MAXIMUM_LOAN_PERIOD;
    }
}
