package ee.taltech.inbankbackend.utils;

import ee.taltech.inbankbackend.exceptions.InvalidPersonalCodeException;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class AgeValidator {

    /**
     * Return age of person in years from provided personal code.
     *
     * @param personalCode Provided personal ID code
     * @return age taken from personal code
     * @throws InvalidPersonalCodeException will be thrown if personal code cannot provide age of the person
     */
    public int getAge(String personalCode) throws InvalidPersonalCodeException {
        int age;
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate birthday = switch (personalCode.charAt(0)) {
            case ('1'), ('2') -> LocalDate.parse(new StringBuilder().append("18").append(personalCode, 1, 7), formatter);
            case ('3'), ('4') -> LocalDate.parse(new StringBuilder().append("19").append(personalCode, 1, 7), formatter);
            case ('5'), ('6') -> LocalDate.parse(new StringBuilder().append("20").append(personalCode, 1, 7), formatter);
            case ('7'), ('8') -> LocalDate.parse(new StringBuilder().append("21").append(personalCode, 1, 7), formatter);
            case ('9'), ('0') -> LocalDate.parse(new StringBuilder().append("22").append(personalCode, 1, 7), formatter);
            default -> null;
        };
        if (birthday == null) {
            throw new InvalidPersonalCodeException("Invalid personal ID code!");
        }
        age = Period.between(birthday, currentDate).getYears();
        return age;
    }
}
