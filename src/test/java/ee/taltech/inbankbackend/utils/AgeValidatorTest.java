package ee.taltech.inbankbackend.utils;

import ee.taltech.inbankbackend.exceptions.InvalidPersonalCodeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class AgeValidatorTest {
    @InjectMocks
    private AgeValidator ageValidator;
    private String tooOldPerson;
    private String averagePerson;

    @BeforeEach
    void setUp() {
        tooOldPerson = "35006069515";
        averagePerson = "50306293719";
    }

    @Test
    void testPersonTooOld() throws InvalidPersonalCodeException {
        assertEquals(73, ageValidator.getAge(tooOldPerson));
    }

    @Test
    void testAveragePerson() throws InvalidPersonalCodeException {
        assertEquals(20, ageValidator.getAge(averagePerson));
    }
}