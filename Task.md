# Conclusion for TICKET-101
This output was created to describe the operation of the "TICKET-101". It will list the strengths and weaknesses of the work, as well as a general description of how the program works. Opinion will also be provided regarding the SOLID principles of classes and functions used in this system.

First of all, it is worth describing the operation of the program in relation to the business requirements provided by the company "Inbank". The code created before me works and copes with its tasks. It meets business requirements. The algorithm for finding client points or "Scoring algorithm" was changed, but this did not degrade the performance of the program. Moreover, it can be assumed that the program has accelerated its productivity. By removing the division from the expression "credit score = (credit modifier / loan amount) * loan period" and changing the if statement condition necessary for making a loan decision, this intern was able to simplify the execution of the program, although the increase in productivity will only be noticeable when executing 100,000+ requests. Other parts of the program have been completed and fully comply with business requirements.

We can say that the strength of this work is its performance and the flow of data in functions and classes. It can also be mentioned that this work tries to follow the principles of "SOLID", but it has problems. The principles "S" and "O" were taken into account by this intern. The remaining principles were ignored. The intern ignored the possibility of creating interfaces and parent classes to improve the quality of work and compliance with the principles of "SOLID". This leads to the problem of being able to change one class to meet changing business requirements. As well, the work has many detailed problems such as incorrectly used data type or class layout structure. I will describe each or almost each of them based on the names of the classes in which these classes are located.

The "requestDecision" function of "DecisionEngineController" class has, in my opinion, some errors. First error is the repeated use of almost the same code. In such cases, it is worth creating a separate function, which will simplify and shorten the code. Below is the code with the error and its corrected version:

##### Code with error
```
/**
 * A REST endpoint that handles requests for loan decisions.
 * The endpoint accepts POST requests with a request body containing the customer's personal ID code,
 * requested loan amount, and loan period.<br><br>
 * - If the loan amount or period is invalid, the endpoint returns a bad request response with an error message.<br>
 * - If the personal ID code is invalid, the endpoint returns a bad request response with an error message.<br>
 * - If an unexpected error occurs, the endpoint returns an internal server error response with an error message.<br>
 * - If no valid loans can be found, the endpoint returns a not found response with an error message.<br>
 * - If a valid loan is found, a DecisionResponse is returned containing the approved loan amount and period.
 *
 * @param request The request body containing the customer's personal ID code, requested loan amount, and loan period
 * @return A ResponseEntity with a DecisionResponse body containing the approved loan amount and period, and an error message (if any)
 */
@PostMapping("/decision")
public ResponseEntity<DecisionResponse> requestDecision(@RequestBody DecisionRequest request) {
    try {
        Decision decision = decisionEngine.
                calculateApprovedLoan(request.getPersonalCode(), request.getLoanAmount(), request.getLoanPeriod());
        response.setLoanAmount(decision.getLoanAmount());
        response.setLoanPeriod(decision.getLoanPeriod());
        response.setErrorMessage(decision.getErrorMessage());

        return ResponseEntity.ok(response);
    } catch (InvalidPersonalCodeException | InvalidLoanAmountException | InvalidLoanPeriodException e) {
        response.setLoanAmount(null);
        response.setLoanPeriod(null);
        response.setErrorMessage(e.getMessage());

        return ResponseEntity.badRequest().body(response);
    } catch (NoValidLoanException e) {
        response.setLoanAmount(null);
        response.setLoanPeriod(null);
        response.setErrorMessage(e.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (Exception e) {
        response.setLoanAmount(null);
        response.setLoanPeriod(null);
        response.setErrorMessage("An unexpected error occurred");

        return ResponseEntity.internalServerError().body(response);
    }
}
```
##### Corrected code
```
/**
 * A REST endpoint that handles requests for loan decisions.
 * The endpoint accepts POST requests with a request body containing the customer's personal ID code,
 * requested loan amount, and loan period.
 * - If the loan amount or period is invalid, the endpoint returns a bad request response with an error message.
 * - If the personal ID code is invalid, the endpoint returns a bad request response with an error message.
 * - If an unexpected error occurs, the endpoint returns an internal server error response with an error message.
 * - If no valid loans can be found, the endpoint returns a not found response with an error message.
 * - If a valid loan is found, a DecisionResponse is returned containing the approved loan amount and period.
 *
 * @param request The request body containing the customer's personal ID code, requested loan amount, and loan period
 * @return A ResponseEntity with a DecisionResponse body containing the approved loan amount and period, and an error message (if any)
 */
@PostMapping("/decision")
public ResponseEntity<DecisionResponse> requestDecision(@RequestBody DecisionRequest request) {
    try {
        Decision decision = decisionEngine.
                calculateApprovedLoan(request.getPersonalCode(), request.getLoanAmount(), request.getLoanPeriod());

        createResponse(decision.getLoanAmount(), decision.getLoanPeriod(), decision.getErrorMessage());

        return ResponseEntity.ok(response);
    } catch (InvalidPersonalCodeException | InvalidLoanAmountException | InvalidLoanPeriodException e) {
        createResponse(null, null, e.getMessage());

        return ResponseEntity.badRequest().body(response);
    } catch (NoValidLoanException e) {
        createResponse(null, null, e.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    } catch (Exception e) {
        createResponse(null, null, "An unexpected error occurred");

        return ResponseEntity.internalServerError().body(response);
    }
}

private void createResponse(Integer loanAmount, Integer loanPeriod, String message) {
    response.setLoanAmount(loanAmount);
    response.setLoanPeriod(loanPeriod);
    response.setErrorMessage(message);
}
```
The "DecisionEngine" class has several errors in my opinion. The first mistake is the use of the "Long" class in writing the signatures of the "calculateApprovedLoan" function instead of the simple "long" data type. However, I should note that this error is not serious. In addition, this function also has a problems with handling the exception that is created, which is misleading. I also rewrote some parts of the function to improve the readability of the program, creating additional functions. As well, it is worth paying attention to the "getCreditModifier" function, since this function provides a credit modifier. If the client is a debtor, we do not issue him a loan. Based on this, this function gives this person a credit modifier equal to 0. After this, the "calculateApprovedLoan" function checks that this modifier is still equal to 0, which is why an exception is thrown. In my opinion, this method is not optimal. This exception can be added to the "getCreditModifier" function, which will speed up the program and make it more readable. Below is the code with the error and its corrected version:
##### Code with error
```
/**
 * Calculates the maximum loan amount and period for the customer based on their ID code,
 * the requested loan amount and the loan period.
 * The loan period must be between 12 and 60 months (inclusive).
 * The loan amount must be between 2000 and 10000€ months (inclusive).
 *
 * @param personalCode ID code of the customer that made the request.
 * @param loanAmount Requested loan amount
 * @param loanPeriod Requested loan period
 * @return A Decision object containing the approved loan amount and period, and an error message (if any)
 * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
 * @throws InvalidLoanAmountException If the requested loan amount is invalid
 * @throws InvalidLoanPeriodException If the requested loan period is invalid
 * @throws NoValidLoanException If there is no valid loan found for the given ID code, loan amount and loan period
 */
public Decision calculateApprovedLoan(String personalCode, Long loanAmount, int loanPeriod)
        throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException,
        NoValidLoanException {
    try {
        verifyInputs(personalCode, loanAmount, loanPeriod);
    } catch (Exception e) {
        return new Decision(null, null, e.getMessage());
    }

    int outputLoanAmount;
    creditModifier = getCreditModifier(personalCode);

    if (creditModifier == 0) {
        throw new NoValidLoanException("No valid loan found!");
    }

    while (highestValidLoanAmount(loanPeriod) < DecisionEngineConstants.MINIMUM_LOAN_AMOUNT) {
        loanPeriod++;
    }

    if (loanPeriod <= DecisionEngineConstants.MAXIMUM_LOAN_PERIOD) {
        outputLoanAmount = Math.min(DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT, highestValidLoanAmount(loanPeriod));
    } else {
        throw new NoValidLoanException("No valid loan found!");
    }

    return new Decision(outputLoanAmount, loanPeriod, null);
}

/**
 * Calculates the credit modifier of the customer to according to the last four digits of their ID code.
 * Debt - 0000...2499
 * Segment 1 - 2500...4999
 * Segment 2 - 5000...7499
 * Segment 3 - 7500...9999
 *
 * @param personalCode ID code of the customer that made the request.
 * @return Segment to which the customer belongs.
 */
private int getCreditModifier(String personalCode) throws NoValidLoanException {
    int segment = Integer.parseInt(personalCode.substring(personalCode.length() - 4));

    if (segment < 2500) {
        return 0;
    } else if (segment < 5000) {
        return DecisionEngineConstants.SEGMENT_1_CREDIT_MODIFIER;
    } else if (segment < 7500) {
        return DecisionEngineConstants.SEGMENT_2_CREDIT_MODIFIER;
    }

    return DecisionEngineConstants.SEGMENT_3_CREDIT_MODIFIER;
}
```
##### Corrected code
```
/**
 * Calculates the maximum loan amount and period for the customer based on their ID code,
 * the requested loan amount and the loan period.
 * The loan period must be between 12 and 60 months (inclusive).
 * The loan amount must be between 2000 and 10000€ months (inclusive).
 *
 * @param personalCode ID code of the customer that made the request.
 * @param loanAmount Requested loan amount
 * @param loanPeriod Requested loan period
 * @return A Decision object containing the approved loan amount and period, and an error message (if any)
 * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
 * @throws InvalidLoanAmountException If the requested loan amount is invalid
 * @throws InvalidLoanPeriodException If the requested loan period is invalid
 * @throws NoValidLoanException If there is no valid loan found for the given ID code, loan amount and loan period
 */
public Decision calculateApprovedLoan(String personalCode, long loanAmount, int loanPeriod)
        throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException,
        NoValidLoanException {

    verifyInputs(personalCode, loanAmount, loanPeriod);

    int outputLoanAmount, highestLoanAmount = highestValidLoanAmount(loanPeriod);
    creditModifier = getCreditModifier(personalCode);

    while (!isLoanAmountBiggerOrEqualToMin(highestLoanAmount)) {
        highestLoanAmount = highestValidLoanAmount(++loanPeriod);
    }

    if (!isLoanPeriodInRange(loanPeriod)) {
        throw new NoValidLoanException("No valid loan found!");
    }

    outputLoanAmount = Math.min(DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT, highestLoanAmount);

    return new Decision(outputLoanAmount, loanPeriod, null);
}

/**
 * Calculates the credit modifier of the customer to according to the last four digits of their ID code.
 * Debt - 0000...2499
 * Segment 1 - 2500...4999
 * Segment 2 - 5000...7499
 * Segment 3 - 7500...9999
 *
 * @param personalCode ID code of the customer that made the request.
 * @return Segment to which the customer belongs or exception that there is no valid loan found for this person.
 */
private int getCreditModifier(String personalCode) throws NoValidLoanException {
    int segment = Integer.parseInt(personalCode.substring(personalCode.length() - 4));

    if (segment < 2500) {
        throw new NoValidLoanException("No valid loan found!");
    } else if (segment < 5000) {
        return DecisionEngineConstants.SEGMENT_1_CREDIT_MODIFIER;
    } else if (segment < 7500) {
        return DecisionEngineConstants.SEGMENT_2_CREDIT_MODIFIER;
    }

    return DecisionEngineConstants.SEGMENT_3_CREDIT_MODIFIER;
}

/**
 * Verify that input (loan period) is valid according to business rules. If so, return true. If otherwise, return false.
 *
 * @param loanPeriod Requested loan period
 * @return answer to the question "is loan period valid?"
 */
private boolean isLoanPeriodInRange(int loanPeriod) {
    return DecisionEngineConstants.MINIMUM_LOAN_PERIOD <= loanPeriod
            && loanPeriod <= DecisionEngineConstants.MAXIMUM_LOAN_PERIOD;
}

/**
 * Verify that input (loan amount) is valid according to business rules.
 * If so, return true. If otherwise, return false.
 *
 * @param loanAmount Requested loan amount
 * @return answer to the question "is loan amount bigger or equal to minimum loan amount?"
 */
private boolean isLoanAmountBiggerOrEqualToMin(long loanAmount) {
    return DecisionEngineConstants.MINIMUM_LOAN_AMOUNT <= loanAmount;
}
```
Finally,  the function "verifyInputs" from the same class "DecisionEngine" has readability issues. Also the code has problems related to the signature. The original and modified code is provided below:
##### Code with error
```
/**
 * Verify that all inputs are valid according to business rules.
 * If inputs are invalid, then throws corresponding exceptions.
 *
 * @param personalCode Provided personal ID code
 * @param loanAmount Requested loan amount
 * @param loanPeriod Requested loan period
 * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
 * @throws InvalidLoanAmountException If the requested loan amount is invalid
 * @throws InvalidLoanPeriodException If the requested loan period is invalid
 */
private void verifyInputs(String personalCode, Long loanAmount, int loanPeriod)
        throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException {
    if (!validator.isValid(personalCode)) {
        throw new InvalidPersonalCodeException("Invalid personal ID code!");
    }
    if (!(DecisionEngineConstants.MINIMUM_LOAN_AMOUNT <= loanAmount)
            || !(loanAmount <= DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT)) {
        throw new InvalidLoanAmountException("Invalid loan amount!");
    }
    if (!(DecisionEngineConstants.MINIMUM_LOAN_PERIOD <= loanPeriod)
            || !(loanPeriod <= DecisionEngineConstants.MAXIMUM_LOAN_PERIOD)) {
        throw new InvalidLoanPeriodException("Invalid loan period!");
    }
}
```
##### Corrected code
```
/**
 * Verify that all inputs are valid according to business rules.
 * If inputs are invalid, then throws corresponding exceptions.
 *
 * @param personalCode Provided personal ID code
 * @param loanAmount Requested loan amount
 * @param loanPeriod Requested loan period
 * @throws InvalidPersonalCodeException If the provided personal ID code is invalid
 * @throws InvalidLoanAmountException If the requested loan amount is invalid
 * @throws InvalidLoanPeriodException If the requested loan period is invalid
 */
private void verifyInputs(String personalCode, long loanAmount, int loanPeriod)
        throws InvalidPersonalCodeException, InvalidLoanAmountException, InvalidLoanPeriodException {
    if (!isPersonalCodeValid(personalCode)) {
        throw new InvalidPersonalCodeException("Invalid personal ID code!");
    }
    if (!isLoanAmountInRange(loanAmount)) {
        throw new InvalidLoanAmountException("Invalid loan amount!");
    }
    if (!isLoanPeriodInRange(loanPeriod)) {
        throw new InvalidLoanPeriodException("Invalid loan period!");
    }
}

/**
 * Verify that input is valid according to country laws.
 * If so, return true. If otherwise, return false. 
 * 
 * @param personalCode Provided personal ID code
 * @return answer to the question "is personal code valid?"
 */
private boolean isPersonalCodeValid(String personalCode) {
    return validator.isValid(personalCode);
}

/**
 * Verify that input (loan amount) is valid according to business rules.
 * If so, return true. If otherwise, return false.
 *
 * @param loanAmount Requested loan amount
 * @return answer to the question "is loan amount valid?"
 */
private boolean isLoanAmountInRange(long loanAmount) {
    return isLoanAmountBiggerOrEqualToMin(loanAmount) && isLoanAmountSmallerOrEqualToMax(loanAmount);
}

/**
 * Verify that input (loan amount) is valid according to business rules.
 * If so, return true. If otherwise, return false.
 *
 * @param loanAmount Requested loan amount
 * @return answer to the question "is loan amount bigger or equal to minimum loan amount?"
 */
private boolean isLoanAmountBiggerOrEqualToMin(long loanAmount) {
    return DecisionEngineConstants.MINIMUM_LOAN_AMOUNT <= loanAmount;
}

/**
 * Verify that input (loan amount) is valid according to business rules.
 * If so, return true. If otherwise, return false.
 *
 * @param loanAmount Requested loan amount
 * @return answer to the question "is loan amount smaller or equal to maximum loan amount?"
 */
private boolean isLoanAmountSmallerOrEqualToMax(long loanAmount) {
    return loanAmount <= DecisionEngineConstants.MAXIMUM_LOAN_AMOUNT;
}

/**
 * Verify that input (loan period) is valid according to business rules.
 * If so, return true. If otherwise, return false.
 *
 * @param loanPeriod Requested loan period
 * @return answer to the question "is loan period valid?"
 */
private boolean isLoanPeriodInRange(int loanPeriod) {
    return DecisionEngineConstants.MINIMUM_LOAN_PERIOD <= loanPeriod
            && loanPeriod <= DecisionEngineConstants.MAXIMUM_LOAN_PERIOD;
}
```

Other classes, like functions, in my opinion, are written well and do not contain errors. I should also note classes related to data storage and exceptions. They are written more than adequately and I have no complaints about them. There is also an opportunity to praise the intern for creating tests to check the functionality of the program.

It is worth finishing with the structure of the program. The program structure may vary. Each company and even programming language has its own structure of classes, files and projects. I have to curse the intern for creating a structure that is divided based on the actions that the program produces. All classes related to the execution of business logic are in the "service" group, classes that receive data from the "Frontend" are in the "endpoint" group, etc.

To summarize, it is worth noting that this intern created a working program that meets all the necessary business requirements. The structure of the program also deserves attention, because it provides the opportunity to work effectively with the program. However, the work has its drawbacks. The logic of the program’s execution creates questions, and some points lead to confusion. The intern's inattention to details such as the types of data used also detracts from the overall assessment of the work. But the intern created documentation for each function and even classes, which allows him to understand the work of the program without outside help.